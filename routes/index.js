var express = require('express');
var router = express.Router();
let controller = require("./controller")

/* GET home page. */
router.get('/', controller.indexPage);
router.get('/internal-data', controller.apiData)
router.get("/page", controller.webPage)

module.exports = router;
