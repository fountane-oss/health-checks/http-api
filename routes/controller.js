let axios = require("axios");
let config = require("../config");

async function indexPage(req, res){
    res.send(`
        <h1>Welcome</h1>
        <a href="/page"> <button>Click Here</button></a> to go to the app
    `);
}

async function webPage(req, res){

    var http_config = {
        method: 'get',
        url: config.endpoint_host + '/internal-data'
    };

    let data = {}
    try {
        data = await axios(http_config)
        data = data.data
    } catch(err){
        console.log(err)
        res.status(200).send(err)
        return
    }
    // console.log("The response is:  ", data)

    let resp = `
      <h1>Welcome to the app </h1>
      <ul>
        <li> <b> version </b> : ${data.version} </li>
        <li> <b> from </b> : ${data.from} </li>
      </ul>

      <a href="/page"> <button>Click Here</button></a> to refresh
    `
    res.status(200).send(resp)
}

async function apiData(req, res){

    res.status(200).json({
        success: true,
        version: config.version,
        from: config.from
    })
}

module.exports = {
    indexPage,
    apiData,
    webPage
}
